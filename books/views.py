from django.shortcuts import render, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from books.models import Books, Post


def home(request):
    books = Books.objects.all().order_by('id')
    paginator = Paginator(books, 8)

    page = request.GET.get('page')

    try:
        books = paginator.page(page)
    except PageNotAnInteger:
        books = paginator.page(1)
    except EmptyPage:
        books = paginator.page(paginator.num_pages)

    return render(request, 'books/home.html', {'books': books})


def post_view(request):
    posts = Post.objects.all()
    return render(request, 'books/post.html', {'posts': posts})


