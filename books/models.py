from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User



class Books(models.Model):
    title = models.CharField(max_length=100, unique=True)
    author = models.CharField(max_length=200, null=True, blank=True)
    category = models.CharField(max_length=50, null=True, blank=True)
    description = models.CharField(max_length=200, null=True, blank=True)
    price = models.CharField(max_length=200, null=True, blank=True)
    isbn10 = models.CharField(max_length=200, null=True, blank=True)
    isbn13 = models.CharField(max_length=200, null=True, blank=True)
    image = models.URLField(name="Image", null=True, blank = True)
    link = models.URLField(name="Book_Link", null=True, blank=True)

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name_plural = "Books"


class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.CharField(max_length=200, null=True, blank=True)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=10,default='draft')

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name_plural = "Posts"
