from django.conf.urls import url
from books import views

urlpatterns = [
    url(r'^$', views.home, name="release"),
    url(r'^post/$', views.post_view, name='post_view'),
]
