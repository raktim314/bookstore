from django.contrib import admin
from books.models import Books, Post


# Register your models here.
admin.site.site_header = 'Book Store Administration'
admin.site.site_title = 'Book Store Administration'
admin.site.register(Books)
admin.site.register(Post)

