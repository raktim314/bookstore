from django.conf.urls import url
from account import views
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^profile/$', views.view_profile, name='view_profile'),
]
