from django.conf.urls import url

from cart import views

urlpatterns = [
    url(r'^add/', views.add_to_cart, name='add_to_cart'),
    url(r'^cart/$', views.view_favourite_books, name='view_favourite_books')

]