from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect

from django.shortcuts import render, redirect
from cart.models import Cart


@login_required
def add_to_cart(request):
    if request.method == 'POST':
        book = request.POST.get("book_id")
        cart = Cart.objects.get_or_create(user=request.user, book_id=book)
        return HttpResponseRedirect('/')


@login_required
def view_favourite_books(request):
    cart_list = Cart.objects.all()
    paginator = Paginator(cart_list, 6)

    page = request.GET.get('page')

    try:
        cart_list = paginator.page(page)
    except PageNotAnInteger:
        cart_list = paginator.page(1)
    except EmptyPage:
        cart_list = paginator.page(paginator.num_pages)
    return render(request, 'books/cart-list.html', {'cart_list': cart_list})