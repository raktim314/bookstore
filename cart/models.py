from django.db import models
from django.contrib.auth.models import User
from books.models import Books


class Cart(models.Model):
    user = models.ForeignKey(User)
    book = models.ForeignKey(Books)

    def __str__(self):
        return str(self.book.title)

