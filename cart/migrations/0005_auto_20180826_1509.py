# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-08-26 09:09
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0004_auto_20180826_0021'),
    ]

    operations = [
        migrations.RenameField(
            model_name='cart',
            old_name='book',
            new_name='books',
        ),
    ]
